Alex Aquino Blog {t-blog}  
---

CRUD JSF Simples utilizando SprigData JPA (CrudRepository).

![t-blog-video-demostracao](/uploads/46156e4f9736cb6af5153cde04610977/t-blog-video-demostracao.mov)

Tecnologias
--

#### Frameworks:

- [Apache Maven](https://maven.apache.org/)
- [JSF 2](https://www.oracle.com/technetwork/java/javaee/javaserverfaces-139869.html)
- [JPA](https://www.oracle.com/technetwork/java/javaee/tech/persistence-jsp-140049.html)
- [Hibernate](http://hibernate.org/)
- [SpringData (CrudRepository)](https://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/repository/CrudRepository.html)
- [Apache Tomcat 9](http://tomcat.apache.org/)
- [PrettyFaces](http://ocpsoft.org/prettyfaces/)
- [Zurb Foundation 6](http://foundation.zurb.com/)
- [PrimeFaces 6](https://primefaces.org/)

#### Base de Dados:

Para permitir que a aplicação possa ser testada sem a necessidade de configuração de um banco de dados externo, 
foi incluido um banco de dados integrado, o H2 Database. 

- [H2 embedded database](http://www.h2database.com/html/main.html). 

Ambiente
---

- Java JDK 8
- Intellij IDEA 2018.2.5
- Apache TomCat 9

Documentação
---

A documentação do software encontra-se em `src/main/resources/documentacao` e inclui:

- Diagrams de Classes
- Manuais
- JavaDoc

Executando a aplicação:
---

1. Realize o clone desse projeto. 
2. Importe o projeto como um projeto Maven em sua IDE preferida (Recomendo Intellij).
3. Configure o seu servidor de aplicação preferido (Recomendo TomCat 9).
4. Execute o projeto.


