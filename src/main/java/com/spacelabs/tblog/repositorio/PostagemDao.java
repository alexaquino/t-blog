package com.spacelabs.tblog.repositorio;

import com.spacelabs.tblog.modelo.Postagem;
import org.springframework.data.repository.CrudRepository;


/**
 * Classe DAO (Data Access Object) responsável pela abstração de acesso aos dados relativos a Postagens.
 *
 * @see Postagem
 *
 * @author e_alexaquino
 * @since 1.0.0
 *
 * */
public interface PostagemDao extends CrudRepository<Postagem, Long> {

    public Postagem findByTituloDaPostagemIgnoreCase(String tituloDaPostagem);

}
