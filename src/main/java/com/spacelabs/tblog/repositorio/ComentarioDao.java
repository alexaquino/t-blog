package com.spacelabs.tblog.repositorio;

import com.spacelabs.tblog.modelo.Comentario;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


/**
 * Classe DAO (Data Access Object) responsável pela abstração de acesso aos dados relativos a Comentários.
 *
 * @see Comentario
 *
 * @author e_alexaquino
 * @since 1.0.0
 *
 * */
public interface ComentarioDao extends CrudRepository<Comentario, Long> {

    public List<Comentario> findByTextoDoComentarioIgnoreCase(String textoDoComentario);

}
