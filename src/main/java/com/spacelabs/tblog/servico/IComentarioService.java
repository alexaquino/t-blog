package com.spacelabs.tblog.servico;

import com.spacelabs.tblog.modelo.Comentario;
import com.spacelabs.tblog.servico.jpa.ComentarioService;

import java.util.List;


/**
 * Interface responsável por especificar o comportamento que {@link ComentarioService} deve implementar.
 *
 * @see Comentario
 *
 * @author e_alexaquino
 * @since 1.0.0
 *
 * */
public interface IComentarioService {

    public List<Comentario> findByTextoDoComentarioIgnoreCase(String textoDoComentario);
    public Comentario findById(Long id);

    public Comentario save(Comentario comentario);
    public void destroy(Comentario comentario);

}
