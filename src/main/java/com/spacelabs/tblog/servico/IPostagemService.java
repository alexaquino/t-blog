package com.spacelabs.tblog.servico;

import com.spacelabs.tblog.modelo.Postagem;
import com.spacelabs.tblog.servico.jpa.PostagemService;

import java.util.List;


/**
 * Interface responsável por especificar o comportamento que {@link PostagemService} deve implementar.
 *
 * @see Postagem
 *
 * @author e_alexaquino
 * @since 1.0.0
 *
 * */
public interface IPostagemService {

    public Postagem findByTituloDaPostagemIgnoreCase(String tituloDaPostagem);
    public List<Postagem> findAll();
    public Postagem findById(Long id);

    public Postagem save(Postagem postagem);
    public void destroy(Postagem postagem);
}
