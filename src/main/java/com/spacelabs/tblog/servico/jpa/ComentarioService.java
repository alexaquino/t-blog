package com.spacelabs.tblog.servico.jpa;

import com.spacelabs.tblog.modelo.Comentario;
import com.spacelabs.tblog.repositorio.ComentarioDao;
import com.spacelabs.tblog.servico.IComentarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Classe responsável por fornecer os serviços de busca, inserção e exclusão de registros referentes aos Comentários.
 *
 * @see ComentarioDao
 * @see IComentarioService
 * @see Comentario
 *
 * @author e_alexaquino
 * @since 1.0.0
 *
 * */
@Service("commentService")
@Repository
@Transactional
public class ComentarioService implements IComentarioService {

    @Autowired
    private ComentarioDao comentarioDao;

    /**
     * Método responsável por recuperar um comentario pelo id no Banco de Dados
     *
     * @param id do comentario a ser recuperado
     * @return comentario recuperado
     */
    @Override
    @Transactional(readOnly = true)
    public Comentario findById(Long id) {
        return comentarioDao.findOne(id);
    }

    /**
     * Método responsável por por recuperar um comentario pelo textoDoComentario no Banco de Dados
     *
     * @param textoDoComentario a ser recuperado
     * @return comentario recuperado
     */
    @Override
    @Transactional(readOnly = true)
    public List<Comentario> findByTextoDoComentarioIgnoreCase(String textoDoComentario) {
        return comentarioDao.findByTextoDoComentarioIgnoreCase(textoDoComentario);
    }

    /**
     * Método responsável por persistir a entidade {@link Comentario} no Banco de Dados
     *
     * @param comentario
     * @return comentario persistido
     */
    @Override
    public Comentario save(Comentario comentario) {
        return comentarioDao.save(comentario);
    }


    /**
     * Método responsável por deletar a entidade {@link Comentario} do Banco de Dados
     *
     * @param comentario a ser deletado
     */
    @Override
    public void destroy(Comentario comentario) {
        comentarioDao.delete(comentario);
    }

}
