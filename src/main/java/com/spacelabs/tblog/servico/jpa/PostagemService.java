package com.spacelabs.tblog.servico.jpa;

import com.google.common.collect.Lists;
import com.spacelabs.tblog.modelo.Postagem;
import com.spacelabs.tblog.repositorio.PostagemDao;
import com.spacelabs.tblog.servico.IPostagemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Classe responsável por fornecer os serviços de busca, inserção e exclusão de registros referentes as Postagens.
 *
 * @see IPostagemService
 * @see PostagemDao
 * @see Postagem
 *
 * @author e_alexaquino
 * @since 1.0.0
 *
 * */
@Service("postService")
@Repository
@Transactional
public class PostagemService implements IPostagemService {

    @Autowired
    private PostagemDao postagemDao;

    /**
     * Método responsável por recuperar todas as postagens presentes no Banco de Dados
     *
     * @return lista de postagens presentes no banco de dados
     */
    @Override
    @Transactional(readOnly = true)
    public List<Postagem> findAll() {
        return Lists.newArrayList(postagemDao.findAll());
    }

    /**
     * Método responsável por recuperar uma postagem pelo id no Banco de Dados
     *
     * @param id da postagem a ser recuperada
     * @return postagem recuperada
     */
    @Override
    @Transactional(readOnly = true)
    public Postagem findById(Long id) {
        return postagemDao.findOne(id);
    }

    /**
     * Método responsável por recuperar uma postagem pelo tituloDaPostagem no Banco de Dados
     *
     * @param tituloDaPostagem a ser recuperada
     * @return postagem recuperada
     */
    @Override
    @Transactional(readOnly = true)
    public Postagem findByTituloDaPostagemIgnoreCase(String tituloDaPostagem) {
        return postagemDao.findByTituloDaPostagemIgnoreCase(tituloDaPostagem);
    }

    /**
     * Método responsável por persistir a entidade {@link Postagem} no Banco de Dados
     *
     * @param postagem a ser persistida
     * @return postagem persistido
     */
    @Override
    @Transactional
    public Postagem save(Postagem postagem) {
        return postagemDao.save(postagem);
    }

    /**
     * Método responsável por deletar a entidade {@link Postagem} do Banco de Dados.
     *
     * @param postagem a ser deletada.
     */
    @Override
    public void destroy(Postagem postagem) {
        postagemDao.delete(postagem);
    }

}
