package com.spacelabs.tblog.modelo;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.core.style.ToStringCreator;

import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;


/**
 * Classe responsável pela representação da entidade Comentario. Os atributos a serem persistidos no banco de dados são: <p/>
 *
 * <ul>
 *   <li><b> autorDoComentario: </b> Autor do Comentário (String).
 *   <li><b> textoDoComentario: </b> Comentário Postado (String).
 * </ul>
 *
 * @author alexaquino
 * @since 1.0.0
 *
 * */
@Entity
@Table(name = "comentarios")
public class Comentario implements Serializable {

    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    private Long id;


    @NotEmpty(message = "Por Favor: Informe seu Nome")
    @Column(name = "autorDoComentario")
    private String autorDoComentario;


    @NotEmpty(message = "Por Favor: Redija seu comentário")
    @Column(name = "textoDoComentario")
    private String textoDoComentario;


    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "postagem_id", referencedColumnName = "id")
    private Postagem postagem;


    /*
     * ----------------------------------------------------------------------------------
     * --- Getters & Setters ------------------------------------------------------------
     * ----------------------------------------------------------------------------------
     * */


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAutorDoComentario() {
        return autorDoComentario;
    }

    public void setAutorDoComentario(String autorDoComentario) {
        this.autorDoComentario = autorDoComentario;
    }

    public String getTextoDoComentario() {
        return textoDoComentario;
    }

    public void setTextoDoComentario(String textoDoComentario) {
        this.textoDoComentario = textoDoComentario;
    }

    public Postagem getPostagem() {
        return postagem;
    }

    public void setPostagem(Postagem postagem) {
        this.postagem = postagem;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("id", getId())
                .append("textoDoComentario", getTextoDoComentario())
                .append("postagem", getPostagem())
                .append("autorDoComentario", getAutorDoComentario())
                .toString();
    }

}
