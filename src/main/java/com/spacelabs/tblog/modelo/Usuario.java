package com.spacelabs.tblog.modelo;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.core.style.ToStringCreator;

import javax.persistence.*;
import java.io.Serializable;

import static javax.persistence.GenerationType.IDENTITY;


// TODO: Implementar regras para Usuario/Login - UsuarioBean, UsuarioDao, UsuarioService...

/**
 * Classe responsável pela representação da entidade Usuario. Os atributos a serem persistidos no banco de dados são: <p/>
 *
 * <ul>
 *   <li><b> login: </b> Login do Usuario (String).
 *   <li><b> senha: </b> Senha de Login (String).
 *   <li><b> isAdmin: </b> Flag que determina se o usuário é ou não um administrador (boolean).
 * </ul>
 *
 * @author alexaquino
 * @since 1.0.0
 *
 * */
@Entity
@Table(name = "usuarios")
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "login")
    @NotEmpty(message = "Por Favor: Informe um login")
    private String login;

    @Column(name = "senha")
    @NotEmpty(message = "Por Favor: Informe uma senha")
    private String senha;

    @Column(name = "isAdmin")
    @NotEmpty
    private boolean isAdmin;


    /*
     * ----------------------------------------------------------------------------------
     * --- Getters & Setters ------------------------------------------------------------
     * ----------------------------------------------------------------------------------
     * */


    public boolean getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("id", getId())
                .append("login", getLogin())
                .append("senha", getSenha())
                .toString();
    }
}
