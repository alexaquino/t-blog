package com.spacelabs.tblog.modelo;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.core.style.ToStringCreator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * Classe responsável pela representação da entidade Postagem. Os atributos a serem persistidos no banco de dados são: <p/>
 *
 * <ul>
 *   <li><b> tituloDaPostagem: </b> Título da Postagem (String).
 *   <li><b> autorDaPostagem: </b> Autor da Postagem (String).
 *   <li><b> textoDaPostagem: </b> Texto da Postagem Postado (String).
 *   <li><b> numeroDecurtidas: </b> Número de curtidas da Postagem (int).
 *   <li><b> numeroDeComentarios: </b> Número de comentarios da Postagem (int).
 *   <li><b> dataDaPostagem: </b> Data da Postagem (DATE).
 * </ul>
 *
 * @author alexaquino
 * @since 1.0.0
 *
 * */
@Entity
@Table(name = "postagens")
public class Postagem implements Serializable {

    private static final long serialVersionUID = 1L;


    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id")
    private Long id;


    @NotEmpty(message = "Por Favor: Informe o título da postagem")
    @Column(name = "tituloDaPostagem")
    private String tituloDaPostagem;


    @NotEmpty(message = "Por Favor: Informe o autor da postagem")
    @Column(name = "autorDaPostagem")
    private String autorDaPostagem;


    @NotEmpty(message = "Por Favor: Redija sua Postagem")
    @Column(name = "textoDaPostagem")
    private String textoDaPostagem;


    @NotNull
    @Column(name = "numeroDeCurtidas")
    private int numeroDeCurtidas;


    @NotNull
    @Column(name = "numeroDeComentarios")
    private int numeroDeComentarios;


    @Temporal(TemporalType.DATE)
    @Column(name = "dataDaPostagem")
    private Date dataDaPostagem = new Date();


    @PrePersist
    private void onCreate() {
        dataDaPostagem = new Date();
    }


    @OneToMany(fetch = FetchType.EAGER, mappedBy = "postagem", cascade = CascadeType.ALL)
    private Set<Comentario> comentarios = new HashSet<>();


    public String getDataDaPostagemFormatada() {
        SimpleDateFormat data = new SimpleDateFormat("dd/MM/yyyy");
        data.setLenient(false);
        return data.format(this.getDataDaPostagem());
    }


    /*
     * ----------------------------------------------------------------------------------
     * --- Getters & Setters ------------------------------------------------------------
     * ----------------------------------------------------------------------------------
     * */


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTituloDaPostagem() {
        return tituloDaPostagem;
    }

    public void setTituloDaPostagem(String tituloDaPostagem) {
        this.tituloDaPostagem = tituloDaPostagem;
    }

    public String getAutorDaPostagem() {
        return autorDaPostagem;
    }

    public void setAutorDaPostagem(String autorDaPostagem) {
        this.autorDaPostagem = autorDaPostagem;
    }

    public String getTextoDaPostagem() {
        return textoDaPostagem;
    }

    public void setTextoDaPostagem(String textoDaPostagem) {
        this.textoDaPostagem = textoDaPostagem;
    }

    public int getNumeroDeCurtidas() {
        return numeroDeCurtidas;
    }

    public void setNumeroDeCurtidas(int numeroDeCurtidas) {
        this.numeroDeCurtidas = numeroDeCurtidas;
    }

    public int getNumeroDeComentarios() {
        return numeroDeComentarios;
    }

    public void setNumeroDeComentarios(int numeroDeComentarios) {
        this.numeroDeComentarios = numeroDeComentarios;
    }

    public Date getDataDaPostagem() {
        return dataDaPostagem;
    }

    public void setDataDaPostagem(Date dataDaPostagem) {
        this.dataDaPostagem = dataDaPostagem;
    }

    public Set<Comentario> getComentarios() {
        return comentarios;
    }

    public void setComentarios(Set<Comentario> comentarios) {
        this.comentarios = comentarios;
    }

    @Override
    public String toString() {
        return new ToStringCreator(this)
                .append("id", getId())
                .append("tituloDaPostagem", getTituloDaPostagem())
                .append("autorDaPostagem", getAutorDaPostagem())
                .append("textoDaPostagem", getTextoDaPostagem())
                .append("numeroDeCurtidas", getNumeroDeCurtidas())
                .append("numeroDeComentarios", getNumeroDeComentarios())
                .append("dataDaPostagem", getDataDaPostagem())
                .toString();
    }
}
