package com.spacelabs.tblog.controle;

import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import com.spacelabs.tblog.modelo.Postagem;
import com.spacelabs.tblog.repositorio.PostagemDao;
import com.spacelabs.tblog.servico.IPostagemService;
import com.spacelabs.tblog.servico.jpa.PostagemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.bean.RequestScoped;
import java.io.Serializable;
import java.util.List;


/**
 * Classe Managed Bean (Controller) responsável pela interação entre a página home (pages/postagens.xhtml)
 * (view) e a camada de negócio (Model).
 *
 * @see PostagemService
 * @see IPostagemService
 * @see PostagemDao
 *
 * @author e_alexaquino
 * @since 1.0.0
 *
 * */
@Component
@RequestScoped
@URLMappings(mappings = {
        @URLMapping(id = "postagens", pattern = "/pages/", viewId = "/faces/pages/postagens.xhtml"),
        @URLMapping(id = "nova-postagem", pattern = "/pages/nova-postagem", viewId = "/faces/pages/nova-postagem.xhtml")
})
public class PostagemBean implements Serializable {

    private static final long serialVersionUID = 1L;


    private List<Postagem> postagens;
    private Postagem postagem;


    @Autowired
    private IPostagemService IPostagemService;


    @PostConstruct
    public void iniciar() {
        this.limpar();
    }

    private void limpar() {
        postagem = new Postagem();
    }


    /**
     * Método responsável por incrementar o numeroDeCurtidas das postagens no Banco de Dados
     *
     * @param postagem
     */
    public void curtir(Postagem postagem) {
        postagem.setNumeroDeCurtidas(postagem.getNumeroDeCurtidas() + 1);
        inserir(postagem);
    }

    /**
     * Método responsável por persistir postagens no Banco de Dados
     *
     * @param postagem
     * @return url da página postagens
     */
    public String inserir(Postagem postagem) {
        IPostagemService.save(postagem);
        this.limpar();
        return "pretty:postagens";
    }

    /**
     * Método responsável por excluir postagens no Banco de Dados
     *
     * @param postagem
     * @return url da página postagens
     */
    public String deletar(Postagem postagem) {
        IPostagemService.destroy(postagem);
        //postagem = new Post();
        return "pretty:postagens";
    }


    /*
     * ----------------------------------------------------------------------------------
     * --- Getters & Setters ------------------------------------------------------------
     * ----------------------------------------------------------------------------------
     * */


    public void setPostagens(List<Postagem> postagens) {
        this.postagens = postagens;
    }

    public List<Postagem> getPostagens() {
        return IPostagemService.findAll();
    }

    public Postagem getPostagem() {
        return postagem;
    }

    public void setPostagem(Postagem postagem) {
        this.postagem = postagem;
    }

}
