package com.spacelabs.tblog.controle;


import com.google.common.collect.Lists;
import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.ocpsoft.pretty.faces.annotation.URLMappings;
import com.spacelabs.tblog.modelo.Comentario;
import com.spacelabs.tblog.modelo.Postagem;
import com.spacelabs.tblog.repositorio.ComentarioDao;
import com.spacelabs.tblog.servico.IComentarioService;
import com.spacelabs.tblog.servico.IPostagemService;
import com.spacelabs.tblog.servico.jpa.ComentarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Classe Managed Bean (Controller) responsável pela interação entre a páginas comentario (pages/comentarios.xhtml)
 * (view) e a camada de negócio (Model).
 *
 * @see IComentarioService
 * @see ComentarioService
 * @see ComentarioDao
 *
 * @author e_alexaquino
 * @since 1.0.0
 *
 * */
@Component
@ManagedBean
@RequestScoped
@URLMappings(mappings={
        @URLMapping(id = "comentarios", pattern = "/pages/#{comentarioBean.postagem_id}/comentarios", viewId = "/faces/pages/comentarios.xhtml")
})
public class ComentarioBean implements Serializable {

    private static final long serialVersionUID = 1L;


    @Autowired
    private IComentarioService IComentarioService;

    @Autowired
    private IPostagemService IPostagemService;


    private Postagem postagem;
    private Comentario comentario;
    private Long postagem_id;
    private List <Comentario> comentarios;


    @PostConstruct
    public void iniciar() {
        comentarios = new ArrayList<>();
        this.limpar();
    }

    private void limpar() {
        comentario = new Comentario();
    }


    @URLAction
    public void loadPost() {
        this.postagem = IPostagemService.findById(postagem_id);
    }

    /**
     * Método responsável por persistir comentarios no Banco de Dados
     *
     * @param comentario
     * @return url da página postagens
     */
    public String inserir(Comentario comentario) {
        postagem.setNumeroDeComentarios(postagem.getComentarios().size() + 1);
        //comentarios.add(comentario);
        IComentarioService.save(comentario);
        //this.limpar();
        return "pretty:postagens";
    }

    /**
     * Método responsável por excluir comentarios do Banco de Dados
     *
     * @param comentario
     * @return url da página comentarios
     */
    public String deletar(Comentario comentario) {
        postagem.setNumeroDeComentarios(postagem.getComentarios().size() - 1);
        IComentarioService.destroy(comentario);
        //comentario = new Comentario();
        return "pretty:comentarios";
    }


    /*
     * ----------------------------------------------------------------------------------
     * --- Getters & Setters ------------------------------------------------------------
     * ----------------------------------------------------------------------------------
     * */


    public Postagem getPostagem() {
        return postagem;
    }

    public void setPostagem(Postagem postagem) {
        this.postagem = postagem;
    }

    public Comentario getComentario() {
        return comentario;
    }

    public void setComentario(Comentario comentario) {
        this.comentario = comentario;
    }

    public Long getPostagem_id() {
        return postagem_id;
    }

    public void setPostagem_id(Long postagem_id) {
        this.postagem_id = postagem_id;
    }

    public List<Comentario> getComentarios() {
        return Lists.newArrayList(postagem.getComentarios());
    }

    public void setComentarios(List<Comentario> comentarios) {
        this.comentarios = comentarios;
    }

}
