package com.spacelabs.tblog.controle;

import com.ocpsoft.pretty.faces.annotation.URLAction;
import com.ocpsoft.pretty.faces.annotation.URLMapping;
import com.spacelabs.tblog.modelo.Postagem;
import com.spacelabs.tblog.repositorio.PostagemDao;
import com.spacelabs.tblog.servico.IPostagemService;
import com.spacelabs.tblog.servico.jpa.PostagemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.io.Serializable;

/**
 * Classe Managed Bean (Controller) responsável pela interação entre a página de edicao de postagem (pages/edicao-postagem.xhtml)
 * (view) e a camada de negócio (Model).
 *
 * @see IPostagemService
 * @see PostagemService
 * @see PostagemDao
 *
 * @author e_alexaquino
 * @since 1.0.0
 *
 * */
@Component
@ManagedBean
@RequestScoped
@URLMapping(id = "edicao-postagem", pattern = "/pages/#{edicaoPostagemBean.id}/edicao", viewId = "/faces/pages/edicao-postagem.xhtml")
public class EdicaoPostagemBean implements Serializable {

    private static final long serialVersionUID = 1L;


    private Long id;
    private Postagem postagem;


    @Autowired
    private IPostagemService IPostagemService;


    @URLAction
    public void loadPost() {
        postagem = IPostagemService.findById(id);
    }


    private void limpar() {
        postagem = new Postagem();
    }

    /**
     * Método responsável por atualizar postagens no Banco de Dados
     *
     * @param postagem
     * @return url da página postagens
     */
    public String atualizar(Postagem postagem) {
        IPostagemService.save(postagem);
        this.limpar();
        return "pretty:postagens";
    }


    /*
     * ----------------------------------------------------------------------------------
     * --- Getters & Setters ------------------------------------------------------------
     * ----------------------------------------------------------------------------------
     * */


    public void setPostagem(Postagem postagem) {
        this.postagem = postagem;
    }

    public Postagem getPostagem() {
        return postagem;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

}
