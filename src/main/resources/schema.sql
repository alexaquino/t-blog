DROP TABLE IF EXISTS postagens;
DROP TABLE IF EXISTS comentarios;
DROP TABLE IF EXISTS usuarios;


CREATE TABLE postagens (
      id INT NOT NULL AUTO_INCREMENT
     , tituloDaPostagem VARCHAR(100) NOT NULL
     , autorDaPostagem VARCHAR(100) NOT NULL
     , textoDaPostagem VARCHAR(1500) NOT NULL
     , dataDaPostagem DATE
     , numeroDeCurtidas INT NOT NULL DEFAULT 0
     , numeroDeComentarios INT NOT NULL DEFAULT 0
     , UNIQUE UQ_POSTAGEM_1 (tituloDaPostagem)
     , PRIMARY KEY (id)
);

CREATE TABLE comentarios (
      id INT NOT NULL AUTO_INCREMENT
     , postagem_id INT NOT NULL
     , textoDoComentario VARCHAR(500) NOT NULL
     , autorDoComentario VARCHAR(100) NOT NULL
     , UNIQUE UQ_COMENTARIO_1 (textoDoComentario)
     , PRIMARY KEY (id)
);

CREATE TABLE usuarios (
      id INT NOT NULL AUTO_INCREMENT
     , login VARCHAR(50) NOT NULL
     , senha VARCHAR(50) NOT NULL
     , isAdmin BIT NOT NULL DEFAULT 0
     , UNIQUE UQ_USURIO_1 (senha)
     , PRIMARY KEY (id)
);
